program text2speech;

{$mode objfpc}{$H+}

uses
  {$IFDEF WIN32}
   if WindowsVersion = wv95 then Result:='Windows 95'
   else if WindowsVersion = wvNT4 then Result:='Windows NT v.4'
   else if WindowsVersion = wv98 then Result:='Windows 98'
   else if WindowsVersion = wvMe then Result:='Windows ME'
   else if WindowsVersion = wv2000 then Result:='Windows 2000'
   else if WindowsVersion = wvXP then Result:='Windows XP'
   else if WindowsVersion = wvServer2003 then Result:='Windows Server 2003'
   else if WindowsVersion = wvVista then Result:='Windows Vista'
   else if WindowsVersion = wv7 then Result:='Windows 7'
   else Result:='Unknown';
  {$ENDIF}

{$IFDEF WIN64}
   if WindowsVersion = wvXP then Result:='Windows XP'
   else if WindowsVersion = wvServer2003 then Result:='Windows Server 2003'
   else if WindowsVersion = wvVista then Result:='Windows Vista'
   else if WindowsVersion = wv7 then Result:='Windows 7'
   else if WindowVersion = wv10 then Result :='Windows 10'
   else if WindowVersion = wv11 then Result :='Windows 11'

   else Result:='Unknown';
  {$ENDIF}
  {$IFDEF UNIX}
   Result:='Darwin';
   cthreads,

  {$ENDIF}
  Classes
  { you can add units after this };

begin
end.

