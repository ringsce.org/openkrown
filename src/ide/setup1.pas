unit setup1;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs;

type
  TForm2 = class(TForm)
  private

  public

  end;

var
  Form2: TForm2;

implementation

{$R *.lfm}

end.

procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := (Length(Edit1.Text) > 0);
end;
