ARG BASE_IMAGE=debian:bullseye

FROM $BASE_IMAGE

# Update.
RUN \
    apt-get update && \
    apt-get -y upgrade

# Upgrade and install all required pkgs
RUN \
    DEBIAN_FRONTEND=noninteractive apt-get install -y apt-utils && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y software-properties-common && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y curl git htop man unzip emacs-nox wget && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y clang fuse libfuse-dev libbz2-1.0 libbz2-dev && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y libbz2-ocaml libbz2-ocaml-dev cmake libgtk2.0-dev && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y libgpmg1-dev fakeroot libncurses5-dev zlib1g-dev && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y libxml2-dev autoconf automake libssl-dev


RUN apt update -y && \
  apt install -y openjdk-11-jre-headless openssh-server git cmake build-essential flex bison automake 
 fpc lazarus-ide 


# Define default command.
CMD ["bash"]
